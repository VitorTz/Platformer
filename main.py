

def main():
    surface = load_surface()
    clock = pygame.time.Clock()
    level = LevelData("01")

    while check_events():
        surface.fill(SURFACE_CFG["color"])
        level.run(surface)
        pygame.display.update()
        clock.tick(60)


if __name__ == "__main__":
    import pygame
    from src.load_surface import load_surface
    from src.events import check_events
    from src.level import LevelData
    from src.settings import SURFACE_CFG
    pygame.init()
    main()

