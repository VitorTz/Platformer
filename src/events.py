from .settings import KEYS
from pygame import event, QUIT, key, KEYUP, KEYDOWN


def change_keys_status(k: key, status: bool) -> None:
    if k in KEYS.keys():
        KEYS[k] = status


def check_events() -> bool:
    for e in event.get():
        if e.type == QUIT:
            return False
        elif e.type == KEYDOWN:
            change_keys_status(e.key, True)
        elif e.type == KEYUP:
            change_keys_status(e.key, False)
    return True

