import json
import os.path
from .tile import Tile
from pygame.sprite import Group
from pygame import Surface


def read_json(path: str) -> any:
    try:
        with open(path, "r") as file:
            return json.load(file)
    except FileNotFoundError:
        return []


class LevelData:

    __tile_group: Group

    def __init__(self, level_number: str):
        self.read_level_data(level_number)

    def read_level_data(self, level_number: str) -> None:
        level = read_json(os.path.join("src/level-data", f"{level_number}.json"))
        self.__tile_group = Group()
        for i, line in enumerate(level):
            for j, tile in enumerate(line):
                if tile == "X":
                    self.__tile_group.add(Tile(pos=(j, i)))

    def run(self, surface: Surface) -> None:
        self.__tile_group.draw(surface)
