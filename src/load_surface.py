from pygame import display, Surface
from .settings import SURFACE_CFG


def load_surface() -> Surface:
    display.set_caption(SURFACE_CFG["name"])
    return display.set_mode((SURFACE_CFG["width"], SURFACE_CFG["height"]))

