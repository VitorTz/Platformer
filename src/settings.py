import pygame

LEVEL_CFG = {
    "lines": 11
}

TILE_CFG = {
    "width": 64,
    "height": 64,
    "color": (230, 230, 230)
}


SURFACE_CFG = {
    "width": 1280,
    "height": LEVEL_CFG["lines"] * TILE_CFG["height"],
    "name": "Platformer",
    "color": (0, 0, 0)
}


VALID_KEYS = (
    pygame.K_RIGHT,
    pygame.K_LEFT,
    pygame.K_DOWN,
    pygame.K_UP,
    pygame.K_SPACE
)

KEYS = {
    key: False for key in VALID_KEYS
}

