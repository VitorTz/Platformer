from pygame import Surface, Rect
from pygame.sprite import Sprite
from .settings import TILE_CFG


class Tile(Sprite):

    def __init__(self, pos: tuple):
        super(Tile, self).__init__()
        self.image = Surface([TILE_CFG["width"], TILE_CFG["height"]])
        self.image.fill(TILE_CFG["color"])
        self.rect = self.image.get_rect()
        self.rect.left, self.rect.top = pos[0] * TILE_CFG["width"], pos[1] * TILE_CFG["height"]
        print(self.rect.left, self.rect.top)
